#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
This code creates an RGB image from the g, r, z bands from the DESI Legacy survey
of just the galaxy in the fits files. It only converts those pixels that are 
equal to or below a specified mag/arcsec^2 surface brightness to color. It does this 
using two different bool masks. Then it overlays the RGB image of the galaxy 
onto the g-band fits file which is inverted.
"""


import numpy as np
from astropy.io import fits
from astropy.visualization import make_lupton_rgb, HistEqStretch, ImageNormalize, LinearStretch, ZScaleInterval, SquaredStretch, MinMaxInterval
import matplotlib.pyplot as plt
import argparse
import cv2
from shapely.geometry import Point
from shapely.affinity import scale
from scipy import ndimage
from tqdm import tqdm
import os
from PIL import Image, ImageEnhance, ImageFilter, ImageOps # pip3 install pillow
import sys
import math

LOCAL_DIR = "/misc_funcs"
IMAN_DIR = os.path.dirname(__file__).rpartition(LOCAL_DIR)[0]

sys.path.append(os.path.join(IMAN_DIR, 'imp/cropping'))

import crop_galaxy_image
import resize_image



def sdss_rgb(imgs, bands, scales=None, m = 0.02):
    #The function below was provided by DESI Legacy and can be found at https://github.com/legacysurvey/imagine/blob/c9cb5d5cbe6023d5d320d2399f98474a51289573/map/views.py#L5383

    
    # This part was modified to only contain the g,r,z bands and ignore the u and i bands.
    rgbscales = {'g': (2,2.5),
                 'r': (1,1.5),
                 'z': (0,0.4), #0.3
                 }
    
    if scales is not None:
        rgbscales.update(scales)

    I = 0
    for img,band in zip(imgs, bands):
        plane,scale = rgbscales[band]
        img = np.maximum(0, img * scale + m)
        I = I + img
    I /= len(bands)
        
    # b,g,r = [rimg * rgbscales[b] for rimg,b in zip(imgs, bands)]
    # r = np.maximum(0, r + m)
    # g = np.maximum(0, g + m)
    # b = np.maximum(0, b + m)
    # I = (r+g+b)/3.
    Q = 20
    fI = np.arcsinh(Q * I) / np.sqrt(Q)
    I += (I == 0.) * 1e-6
    H,W = I.shape
    rgb = np.zeros((H,W,3), np.float32)
    for img,band in zip(imgs, bands):
        plane,scale = rgbscales[band]
        rgb[:,:,plane] = (img * scale + m) * fI / I

    # R = fI * r / I
    # G = fI * g / I
    # B = fI * b / I
    # # maxrgb = reduce(np.maximum, [R,G,B])
    # # J = (maxrgb > 1.)
    # # R[J] = R[J]/maxrgb[J]
    # # G[J] = G[J]/maxrgb[J]
    # # B[J] = B[J]/maxrgb[J]
    # rgb = np.dstack((R,G,B))
    rgb = np.clip(rgb, 0, 1)
    return rgb


def read_this(image_file, gray_scale=False):
    image_src = cv2.imread(image_file)
    if gray_scale:
        image_src = cv2.cvtColor(image_src, cv2.COLOR_BGR2GRAY)
    else:
        image_src = cv2.cvtColor(image_src, cv2.COLOR_BGR2RGB)
    return image_src



def create_elliptical_mask(shape, center, semi_major, semi_minor):
    y, x = np.ogrid[:shape[0], :shape[1]]
    ellipse = Point(center).buffer(1.0)  # create a circle
    ellipse = scale(ellipse, semi_major, semi_minor)  # scale to ellipse

    mask = np.zeros(shape, dtype=bool)

    # Create a mask within a square containing the galaxy ellipse (works for arbitrary galaxy PA)
    for i in range(int(center[1])-semi_major, int(center[1])+semi_major+1, 1):
        for j in range(int(center[0])-semi_major, int(center[0])+semi_major+1, 1):
            if ellipse.contains(Point(j, i)):
                mask[i, j] = True

    return mask



def process_image(g_image, r_image, z_image, SMA, SMB, output_file, text=None, pix2sec=0.262, m0=22.5, hor_pos=0.03, vert_pos=0.94, factor=None, SB_level=25.0, Brightness_factor=0.75, blur_window=11):

    if factor is not None:
        # We crop the image within a square box [xc-factor*SA:xc+factor*SA,yc-factor*SA:yc+factor*SA]
        # The galaxy must be centered!!
        # The initial galaxy image must have the same x and y dimensions

        for input_image in [g_image, r_image, z_image]:
            crop_galaxy_image.main(input_image, factor*SMA, factor*SMA, xc=None, yc=None, output_image=input_image.split('.fits')[0]+'_tmp.fits', PA=0., hdu=0, method='Astropy',
            square=False, galaxy_region=None)
            if input_image==g_image:
                g_fits = fits.open(input_image.split('.fits')[0]+'_tmp.fits')
            if input_image==r_image:
                r_fits = fits.open(input_image.split('.fits')[0]+'_tmp.fits')
            if input_image==z_image:
                z_fits = fits.open(input_image.split('.fits')[0]+'_tmp.fits')
            os.remove(input_image.split('.fits')[0]+'_tmp.fits')
    else:
        # Load original FITS data without cropping
        g_fits = fits.open(g_image)
        r_fits = fits.open(r_image)
        z_fits = fits.open(z_image)
    
    g_data = g_fits[0].data
    r_data = r_fits[0].data
    z_data = z_fits[0].data
    
    header = g_fits[0].header
    ny, nx = np.shape(g_data)

    # Galaxy center is at the center of the image
    cen_x = float(nx)/2.
    cen_y = float(ny)/2.

    # Apply Gaussian filter to smooth the data
    g_data_smoothed = ndimage.gaussian_filter(g_data, sigma=3.0, order=0)

    # Calculate surface brightness and mask pixels fainter than the specified SB_level in mag/arcsec^2
    surface_brightness_mask = np.zeros_like(g_data, dtype=bool)

    surface_brightness_mask[(g_data_smoothed>0) & (m0 - 2.5 * np.log10(g_data_smoothed) + 5. * np.log10(pix2sec)<=SB_level)] = True
    
    # Create elliptical mask centered on the galaxy
    elliptical_mask = create_elliptical_mask(g_data.shape, (cen_x, cen_y), int(math.ceil(SMA)), int(math.ceil(SMB)))
    
    # Apply the elliptical mask to each channel
    g_ellip_masked = np.where(elliptical_mask, g_data, 0)
    r_ellip_masked = np.where(elliptical_mask, r_data, 0)
    z_ellip_masked = np.where(elliptical_mask, z_data, 0)
    
    imgs = [g_ellip_masked, r_ellip_masked, z_ellip_masked]
    bands = ['g', 'r', 'z']
    
    #Creates a color image from the different bands using different scaling for each band, values and code found at https://github.com/legacysurvey/imagine/blob/main/map/views.py#L5428
    rgb_image = sdss_rgb(imgs, bands, scales=dict(g=(2,6.0), r=(1,3.4), z=(0,2.2)), m=0.03)

    # Split the RGB to enhance each layer
    r_image, g_image, b_image = cv2.split(rgb_image)
    
    #Apply the surface brightness mask
    r_masked = np.where(surface_brightness_mask, r_image, 0)
    g_masked = np.where(surface_brightness_mask, g_image, 0)
    b_masked = np.where(surface_brightness_mask, b_image, 0)


    # Merge the masked channels back
    rgb_image_masked = cv2.merge((r_masked, g_masked, b_masked))

    # Invert the g-band fits file so bright objects appear black
    #g_data = g_data_smoothed # This significantly changes the background!!
    g_inverted = np.max(g_data) - g_data

    # Normalize and apply histogram stretch to the g-band data
    norm = ImageNormalize(stretch=HistEqStretch(g_inverted))
    g_normalized = norm(g_inverted)/Brightness_factor # Brightness_factor changes brightness: smaller->brighter

    # Blur the image to highlight faint details
    g_normalized = cv2.GaussianBlur(g_normalized, (blur_window, blur_window), 0)

    # Create final image by overlaying RGB on g-band
    final_image = np.dstack([g_normalized]*3)  # Make a 3-channel image from the normalized g-band

    # Combine masks
    combined_mask = np.logical_and(elliptical_mask, surface_brightness_mask)
    
    # Applying the combined mask keeps the ellipse from appearing as a black shape with the color galaxy superimposed over it.
    final_image[combined_mask] = rgb_image_masked[combined_mask] / float(np.max(rgb_image_masked))  # Apply the mask and normalize the RGB image since the g-band image has been normalized


    # Plot and save the final image
    fig, ax = plt.subplots(figsize=(10, 10))
    ax.imshow(final_image, origin='lower')#, cmap='Greys')

    ax.axis('off')

    # This bit of code is only if we decide to include text in the image. Code can be added so that this works but for now there is no user option for it.
    if text:
        ax.text(hor_pos, vert_pos, text, fontsize=25, color='lime', transform=ax.transAxes,
                horizontalalignment='left', verticalalignment='baseline', backgroundcolor='black')

    size_x_arcmin = round(pix2sec * nx / 60.,1)
    size_y_arcmin = round(pix2sec * ny / 60.,1)

    ax.text(0.03, 0.03, '%.1fx%.1f arcmin' % (size_x_arcmin,size_y_arcmin), fontsize=25, color='white', transform=ax.transAxes,
                horizontalalignment='left', verticalalignment='baseline', backgroundcolor='gray')


    plt.savefig(output_file, bbox_inches='tight', pad_inches=0.01, dpi=300)
    plt.clf()
    plt.close()


def main(g_image, r_image, z_image, SMA, SMB, output_file, text=None, pix2sec=0.262, m0=22.5, hor_pos=0.03, vert_pos=0.94, factor=None, SB_level=25.0, Brightness_factor=0.75, blur_window=11):
    process_image(g_image, r_image, z_image, SMA, SMB, output_file, text=text, pix2sec=pix2sec, m0=m0, hor_pos=hor_pos, vert_pos=vert_pos, factor=factor, SB_level=SB_level, Brightness_factor=Brightness_factor, blur_window=blur_window)
    print('Done: %s, %s, %s have been combined' % (g_image, r_image, z_image))



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--g', help="Input path to the g-band image", type=str, required=True)
    parser.add_argument('--r', help="Input path to the r-band image", type=str, required=True)
    parser.add_argument('--z', help="Input path to the z-band image", type=str, required=True)
    parser.add_argument('--o', help="Output file", type=str, required=True)
    parser.add_argument('--sma', help="Semi-major axis", type=float, required=True)
    parser.add_argument('--smb', help="Semi-minor axis", type=float, required=True)
    parser.add_argument('--t', nargs='?', const=1, help="Optional: overlaid text (galaxy name)", type=str, default= None)
    parser.add_argument('--f', nargs='?', const=1, help="Optional: factor to crop the galaxy of of the original image using its semi-major and -minor axes", type=float, default= None)

    args = parser.parse_args()

    g_image = args.g
    r_image = args.r
    z_image = args.z
    output_file = args.o

    text = args.t
    factor = args.f

    SMA = args.sma
    SMB = args.smb
    main(g_image, r_image, z_image, SMA, SMB, output_file, text=None, factor=None)
