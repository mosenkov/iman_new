from astropy.stats import SigmaClip
from photutils.background import Background2D, MedianBackground
from astropy.io import fits
from astropy.stats import sigma_clipped_stats, SigmaClip
from photutils.segmentation import detect_threshold, detect_sources
from photutils.utils import circular_footprint
from photutils.segmentation import (detect_sources,
                                    make_2dgaussian_kernel)
import numpy as np
from astropy.convolution import convolve
import sys
import os
import argparse

import platform
opsyst = platform.system()



LOCAL_DIR = "/misc"
IMAN_DIR = os.path.dirname(__file__).rpartition(LOCAL_DIR)[0]

sys.path.append(os.path.join(IMAN_DIR, 'imp/rebinning'))
sys.path.append(os.path.join(IMAN_DIR, 'imp/sky_fitting'))
sys.path.append(os.path.join(IMAN_DIR, 'imp/masking'))
sys.path.append(os.path.join(IMAN_DIR, 'imp/cropping'))
sys.path.append(os.path.join(IMAN_DIR, 'imp/ds9_regions'))


import rebin_image
import determine_sky
import auto_masking
import convert_reg_to_mask
#import backg_subtr
import crop_image
import subprocess
#import spline_bkg_subtr
import arithm_operations
import convert_fk5_to_image

def convert_segm_to_boolean(mask):
    return np.ma.make_mask(mask, shrink=False)

def main(input_image, reference_image, region_file, calib_region, output_image):
    # Convert region file to fits file (mask)
    convert_reg_to_mask.mask(input_image, region_file, output_image=None, output_mask='mask_replace_tmp.fits', mask_value=1, verbosity=True, mask_DN=None)
    
    hdulist_inp = fits.open(input_image)
    img_inp = hdulist_inp[0].data
    header_inp = hdulist_inp[0].header    
    
    hdulist_ref = fits.open(reference_image)
    img_ref = hdulist_ref[0].data
    header_ref = hdulist_ref[0].header

    hdulist_mask = fits.open('mask_replace_tmp.fits')
    img_mask = hdulist_mask[0].data
    
    '''
    subprocess.call("python3 %s/mto.py %s -out %s -bg_mean 0. -verbosity 0" % (os.path.join(IMAN_DIR, 'imp/masking/mto-lib'),input_image,'segm_mto.fits'), shell=True)

    hdulist_mask_mto = fits.open('segm_mto.fits')
    mask = hdulist_mask_mto[0].data
    mask[np.isnan(img_inp)]=1
    mask[np.isnan(img_ref)]=1
    mask[mask!=-1]=1
    mask[mask==-1]=0
    #outHDU = fits.PrimaryHDU(mask, header=header_inp)
    #outHDU.writeto('test.fits', overwrite=True)          
    #exit()
    os.remove('segm_mto.fits')
      
    # Create astropy mask (boolean)
    mask_astropy = convert_segm_to_boolean(mask)
    
    
    mean, median, std = sigma_clipped_stats(img_inp/img_ref, sigma=3.0, mask=mask_astropy)
    print(median)
    '''
    convert_reg_to_mask.mask(input_image, calib_region, output_image=None, output_mask='calib_tmp.fits', mask_value=1, verbosity=True, mask_DN=None)
    hdulist_mask_calib = fits.open('calib_tmp.fits')
    img_mask_calib = hdulist_mask_calib[0].data
    
    print(np.sum(img_inp[img_mask_calib==1]))
    calib_coeff = np.sum(img_inp[img_mask_calib==1])/np.sum(img_ref[img_mask_calib==1])
    print(calib_coeff)
    
    img_inp[img_mask==1] = img_ref[img_mask==1]*calib_coeff
    
    outHDU = fits.PrimaryHDU(img_inp, header=header_inp)
    outHDU.writeto(output_image, overwrite=True)       


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Convert region file with a mask to a fits segmentation image")
    parser.add_argument("input_image", help="Input fits image to be fixed")
    parser.add_argument("reference_image", help="Input reference image")
    parser.add_argument("region_file", help="Input region file with polygons to be replaced")
    parser.add_argument("calib_region_file", help="Region for calibration (circle on a bright non-saturated star)")
    parser.add_argument("output_image", help="Output image")
    args = parser.parse_args()

    input_image = args.input_image
    reference_image = args.reference_image
    output_image = args.output_image
    region_file = args.region_file
    calib_region = args.calib_region_file  

    main(input_image, reference_image, region_file, calib_region, output_image)

#reference_image = '/media/mosav/MY_DATA_DRIVE/APO_observations_red/UT240412/reduced/UGC10043.g.1_astro_rebin.fits'
#input_image = '/media/mosav/MY_DATA_DRIVE/APO_observations_red/UT240412/reduced/UGC10043.g.5_astro.fits'
#output_image = '/media/mosav/MY_DATA_DRIVE/APO_observations_red/UT240412/reduced/UGC10043.g.5_astro_corr.fits'
#region_file = '/media/mosav/MY_DATA_DRIVE/APO_observations_red/UT240412/reduced/streaks.reg' 
#main(input_image, reference_image, region_file, output_image)
    
