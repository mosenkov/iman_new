
from PIL import Image, ImageEnhance, ImageFilter, ImageOps

def main(input_image, output_image, resolution=300):
    basewidth = resolution
    img = Image.open(input_image)
    wpercent = (basewidth/float(img.size[0]))
    hsize = int((float(img.size[1])*float(wpercent)))
    try:
        img = img.resize((basewidth,hsize), Image.ANTIALIAS)
    except:
        img = img.resize((basewidth,hsize), Image.Resampling.LANCZOS)
    img = img.convert("RGB") # Remove this if you want png!!!!
    img.save(output_image)
