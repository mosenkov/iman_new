import sys
import astropy.io.fits as fits
import os
import scipy

LOCAL_DIR = "/imred"
IMAN_DIR = os.path.dirname(__file__).rpartition(LOCAL_DIR)[0]

sys.path.append(os.path.join(IMAN_DIR, 'imp/rebinning'))
sys.path.append(os.path.join(IMAN_DIR, 'imp/sky_fitting'))
sys.path.append(os.path.join(IMAN_DIR, 'imp/masking'))
sys.path.append(os.path.join(IMAN_DIR, 'imp/cropping'))
sys.path.append(os.path.join(IMAN_DIR, 'plotting/2dprofile'))
sys.path.append(os.path.join(IMAN_DIR, 'sextractor/'))

import photometric_calibration
import remove_cosmic_rays
import rebin_image
import determine_sky
import auto_masking
import convert_reg_to_mask
import backg_subtr
import crop_image
import photutil_bkg
import glob
import numpy as np
import argparse
import subprocess
import shutil
import ccdproc
import test_for_deepness
import plot_smoothed_image
import run_SExtractor
import read_SEcat
import useful_functions as ufoo
from matplotlib import pyplot as plt


# visualizatiom

from astropy.visualization import (MinMaxInterval, LogStretch,
                                   ImageNormalize)
from astropy.visualization import ZScaleInterval, PercentileInterval



def main(input_image):
    name = input_image.stem
    header = fits.open(input_image)[0].header

    try:
        pix2sec = header['PIXSCALE_NEW']
    except:
        pix2sec = float(input('Enter the image pixscale\n'))

    run_SExtractor.call_SE(input_image=str(input_image), sextr_setup='cold.sex', output_cat='photometry.cat', checkimage_type='SEGMENTATION,APERTURES', checkimage_name='%s_segm.fits,%s_aper.fits' %( name, name), m0=0.0, pix2sec=pix2sec)
    
    columns = read_SEcat.find_sex_column(sex_catalog='photometry.cat', col_names=['X_IMAGE', 'Y_IMAGE', 'FLUX_AUTO', 'NUMBER', 'CLASS_STAR', 'FWHM_IMAGE'], dtypes=str)
    
    x    = np.array([float(a) for a in columns[0]])
    y    = np.array([float(a) for a in columns[1]])
    mag  = -2.5*np.log10(np.array([float(a) for a in columns[2]]))
    nums = np.array([float(a) for a in columns[3]])
    cs   = np.array([float(a) for a in columns[4]])
    fwhm = np.array([float(a) for a in columns[5]])
    
    inds = []
    first = []
    for i in range(len(nums)):
        if not(nums[i] in first):
            inds.append(i)
            first.append(nums[i])
            continue
    inds = np.array(inds)

    x = x[inds]
    y = y[inds]
    mag = mag[inds]
    nums = nums[inds]
    cs   = cs[inds]
    fwhm = fwhm[inds]

    xy = [(xt, yt) for xt, yt in zip(x, y)]
    return xy, mag, nums, cs, fwhm


def save_sextr_params(fname, nums):
    reg = open('aper_auto.reg', 'w')
    i_s = []

    print('# Region file format: DS9 version 4.1 \n global color=green dashlist=8 3 width=1 font="helvetica 10 normal roman" select=1 highlite=1 dash=0 fixed=0 edit=1 move=1 delete=1 include=1 source=1\n image', file=reg)
    with open(fname, 'r') as f:
        for line in f.readlines():
            if line[0] == "#":
                continue
            i = int(line.split()[0])
            if i in i_s:
                continue
            i_s.append(i)
            
            if i in nums:
                x = float(line.split()[1])
                y = float(line.split()[2])
                A  = float(line.split()[4])
                B  = float(line.split()[5])
                Kr = float(line.split()[8])
                Theta = float(line.split()[6])

                print('ellipse(%s,%s,%s,%s,%s)' %(x, y, A*Kr, B*Kr, Theta), file=reg)
                print('point(%s, %s) # point=x' %(x,y), file=reg)

    return
    
