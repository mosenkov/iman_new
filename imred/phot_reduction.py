import move_phot_sys    as mps
import useful_functions as ufoo
import photometry_with_SExtractor as pws
import settings         

from pathlib import Path
from astropy.wcs import WCS
import subprocess
import numpy as np
import argparse
import os
from matplotlib import pyplot as plt
from astropy.visualization import ZScaleInterval, PercentileInterval
from astropy.visualization import (MinMaxInterval, LogStretch,
                                   ImageNormalize)

def main(fnames, bdr=0, low_mag=14, up_mag=17, fwhm=9.0, k0=2, k1=2.5, k2=3, 
            path_to_mask=None, catalog='NOMAD', manual=True, exptime=None, filters='BVR', out_dir='.', app=None, out_file='final_image_corr.fits', show=False, cs0=0.9, dist=24):

    sets = settings.Settings()

    RAc, DECc, rc = ufoo.get_image_center(fnames)

    if len(fnames) == 1:
        f = filters[0]
        filters = 3*filters
    else:
        f = filters[1]

    if catalog in sets.known_catalogs: 
        print('Getting data from the star catalogues. This may take some time...')
        df = ufoo.load_stars_from_Vizier(RAc, DECc, rc, low_mag=low_mag, up_mag=up_mag, filt=f,
                          out_file=Path(out_dir, 'input_catalog.csv'), catalog=[catalog])
    else:        
        print('I do not know such a catalog... Exit.')
        os._exit(1)

    mags   = []
    fwhms  = []
    coords = []
    first  = True
    
    for fname in fnames:
    
        data, header = ufoo.get_file(fname)
        gal = fname.stem #fname.split('-')[0].split('/')[-1]
        
        if path_to_mask is None:
            pass
        else:
            mask, _ = ufoo.get_file(path_to_mask)
            mask = mask >= 1
            data[mask] = float('nan')
        
        if first:
            h, w = data.shape
            W = WCS(header)
            
            xy_c, Bmag, Vmag, Rmag      = ufoo.get_stars_for_phot(df, W, bdr, w, h, catalog, filters)
            xy_p, mag, nums, cs, fwhms   = pws.main(Path(fname))
            inds_c, inds_p              = ufoo.merge_catalogues_segmentation(xy_c, xy_p, nums, Path(fname), dist)

            xy_c = xy_c[inds_c]
            Bmag = Bmag[inds_c]

            xy_m = np.array([xy_p[i] for i in inds_p])
            Best = np.array([mag[i] for i in inds_p])
            nums = np.array([nums[i] for i in inds_p])
            cs   = np.array([cs[i] for i in inds_p])
            fwhms   = np.array([fwhms[i] for i in inds_p])

            
            x_tmp = [a[0] for a in xy_m]
            y_tmp = [a[1] for a in xy_m]
            _, inds_tmp = ufoo.del_nearest(x_tmp, y_tmp, ap=k2*np.mean(fwhms))
            xy_c   = xy_c[inds_tmp]
            xy_m   = xy_m[inds_tmp]
            Bmag = Bmag[inds_tmp]
            Best = Best[inds_tmp]
            nums = nums[inds_tmp]
            cs   = cs[inds_tmp]
            fwhms   = fwhms[inds_tmp]
    
            # Delete bad CLASS_STARS
            inds_tmp = np.where(cs > cs0)
            xy_c   = xy_c[inds_tmp]
            xy_m   = xy_m[inds_tmp]
            Bmag  = Bmag[inds_tmp]
            Best  = Best[inds_tmp]
            nums  = nums[inds_tmp]
            fwhms = fwhms[inds_tmp]
            cs    = cs[inds_tmp]
            
            
            # Delete bad Bmag - Best
            inds_tmp = ufoo.clean(Bmag - Best)
            xy_c   = xy_c[inds_tmp]
            xy_m   = xy_m[inds_tmp]
            Bmag  = Bmag[inds_tmp]
            Best  = Best[inds_tmp]
            nums  = nums[inds_tmp]
            fwhms = fwhms[inds_tmp]
            cs    = cs[inds_tmp]
            
            # Delete bad FWHM
            inds_tmp = ufoo.clean(fwhms)
            xy_c   = xy_c[inds_tmp]
            xy_m   = xy_m[inds_tmp]
            Bmag  = Bmag[inds_tmp]
            Best  = Best[inds_tmp]
            nums  = nums[inds_tmp]
            fwhms = fwhms[inds_tmp]
            cs    = cs[inds_tmp]
            print('FWHM=%s +- %s' %(np.round(np.mean(fwhms),2), np.round(np.std(fwhms),2)))

            print('%s stars' %len(cs))

            pws.save_sextr_params('photometry.cat', nums)

            with open('photometry.dat', 'w') as f:
                print('#num x y m_cat m_inst fwhm cs', file=f)
            
                for i, xy, mc, mi, fwhm_, cs_ in zip(nums, xy_m, Bmag, Best, fwhms, cs):
                    print(i, xy[0], xy[1], mc, mi, fwhm_, cs_, file=f)

            subprocess.call('rm input_catalog.csv photometry.cat', shell=True)

            #plt.figure()
            #norm = ImageNormalize(data, interval=PercentileInterval(99),stretch=LogStretch(100))
            #plt.imshow(data, origin='lower', norm=norm)
            #plt.plot([a[0] for a in xy_c], [a[1] for a in xy_c], '+r')
            #plt.plot([a[0] for a in xy_m], [a[1] for a in xy_m], 'xb')
            #plt.show()


    if len(fnames) == 1:
        mps.get_equals_solo(m_calib=Bmag, m_inst=Best, fname=fnames[0], filt=filters[0], out_dir=out_dir, inds0=[], out_file=out_file)
        return

def run(args):
   
    fnames = []
    file1 = Path(args.first_file)
    fnames.append(file1)
    
    file2 = args.second_file
    if file2 == 'N':
        pass
    else:
        file2 = Path(args.second_file)
        fnames.append(file2)
    file3 = args.third_file
    if file3 == 'N':
        pass
    else:
        file3 = Path(args.third_file)
        fnames.append(file3)

    catalog = args.catalog
    filters = args.filters
    low_mag = args.low_mag
    up_mag  = args.up_mag 
    bdr     = args.bdr
    fwhm    = args.fwhm
    k0      = args.k0
    k1      = args.k1
    k2      = args.k2
    manual  = args.manual
    p2m     = args.path_to_mask
    app     = args.aperture_file

    if p2m is None:
        pass
    else:
        p2m = Path(p2m)

    exptime = None
    out_dir = args.path_to_out_dir
    if out_dir is None:
        result_dir = Path('.')
    else:
        if os.path.exists(out_dir):
            out_dir = Path(args.path_to_out_dir)
        else:
            key = True
            while key:
                ans = input("No path %s. Do you want to create path? (y/n) \n" %out_dir)

                if (ans == "y"):
                    subprocess.run("mkdir -p %s" %out_dir, shell=True)  
                    key = not(key)
                    out_dir = Path(args.path_to_out_dir)
                elif (ans == "n"):
                    print("Exit...")
                    key = not(key)
                    return
    echo = True    
    print('===INPUT_PARAMETERS===')
    if echo:
        ufoo.echo(args)
    print('===START===')

    main(fnames=fnames, filters=filters, bdr=bdr, low_mag=low_mag, up_mag=up_mag, fwhm=fwhm, k0=k0, k1=k1, k2=k2, 
            path_to_mask=p2m, catalog=catalog, manual=manual, exptime=exptime, out_dir=out_dir, app=app)



#-------------------------------------------------------------------------------------------

#if __name__ == '__main__':
#    os.chdir('../')
#    gals = ['M100']#['UGC9560','NGC7743','NGC6181','NGC5660','NGC5430','NGC5371','NGC3583','NGC895' ,'M100']
#    for gal in gals:
#        main(['WMO/bkg_est_result/%s-B.fits' %gal, 'WMO/bkg_est_result/%s-V.fits' %gal,'WMO/bkg_est_result/%s-R.fits' %gal])

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    
    parser.add_argument("first_file", type=str, help='First fits file')
    parser.add_argument("second_file", type=str, help='Second fits file')
    parser.add_argument("third_file", type=str,  help='Third fits file') 
    parser.add_argument("-c", "--catalog", type=str, default="NOMAD", help='Catalog of standard stars. Currently available: "SDSS", "NOMAD", "PS1" ')
    parser.add_argument("-f", "--filters", type=str, default="BVR",  help='Enter three filters for each image, respectively. Enter together, for example, "BVR" for the "NOMAD" catalog')
    parser.add_argument("-b", "--bdr", type=int, default=100, help="Indent from the edge of the image [pix]")
    parser.add_argument("-lm", "--low_mag", type=float, default=14.0, help="Filtering stars by stellar magnitude. low_mag < V (g) < up_mag")
    parser.add_argument("-um", "--up_mag", type=float, default=17.0, help="Filtering stars by stellar magnitude. low_mag < V (g) < up_mag")
    parser.add_argument( "--fwhm", type=float, default=9.0, help="FWHM of stars [pix]")
    parser.add_argument("--k0", type=float, default=2.0, help="The radius of the aperture is k0*fwhm")
    parser.add_argument("--k1", type=float, default=2.5, help="The inner radius of the annulus is k1*fwhm")
    parser.add_argument("--k2", type=float, default=3.0, help="The outer radius of the annulus is k2*fwhm")
    parser.add_argument("--manual", action='store_true',  help='Enabling manual mode')
    parser.add_argument("-p2m", "--path_to_mask", default=None,  help='Path to mask file')
    parser.add_argument("-out", "--path_to_out_dir", type=str, default='.',  help='Path to output directory')
    parser.add_argument('-app', '--aperture_file', type=str, default=None, help='ds9.reg file with apertures')


    
    args = parser.parse_args()
    run(args)
